# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .models import Airport, Aircraft, Webservice, Flight, AirportDistance
from .serializers import AirportSerializer, AircraftSerializer, WebserviceSerializer, FlightSerializer, AirportByStateSerializer, AirportDistanceSerializer
from rest_framework import generics
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Count
from .services import get_airport_closer, get_airport_further

# Create your views here.
class WebserviceListCreate(generics.ListCreateAPIView):
  queryset = Webservice.objects.all()
  serializer_class = WebserviceSerializer

class AirportListCreate(generics.ListCreateAPIView):
  queryset = Airport.objects.all()
  serializer_class = AirportSerializer

class AircraftListCreate(generics.ListCreateAPIView):
  queryset = Aircraft.objects.all()
  serializer_class = AircraftSerializer

class FlightListCreate(generics.ListCreateAPIView):
  queryset = Flight.objects.order_by("-airport_dist__distance")
  serializer_class = FlightSerializer

class AirportByState(generics.ListCreateAPIView):
  """
  Return all states and airport quantities
  """
  queryset = Airport.objects.values('state').annotate(scount=Count('state')).order_by('-scount')
  serializer_class = AirportByStateSerializer

class Questao3(APIView):
  """
  Return all flight origin with closer and further destinations
  """
  renderer_classes = [JSONRenderer]

  def get(self, request, format=None):
    airports = Airport.objects.all()[0:20]
    count = 0
    result = []
    for airport in airports:
      count += 1

      max = get_airport_further(airport.id)
      min = get_airport_closer(airport.id)
      
      if max and min:
        airport_max = Airport.objects.get(pk=max[0])
        airport_max_distance = AirportDistance.objects.get(airport_from=airport, airport_to=airport_max)

        airport_min = Airport.objects.get(pk=min[0])
        airport_min_distance = AirportDistance.objects.get(airport_from=airport, airport_to=airport_min)

        flight_max = Flight.objects.filter(airport_from=airport, airport_to=airport_max).count()
        flight_min = Flight.objects.filter(airport_from=airport, airport_to=airport_min).count()
        if flight_max > 0 and flight_min > 0:
          result.append({ 'airport_from': AirportSerializer(airport).data, 'airport_max': AirportDistanceSerializer(airport_max_distance).data, 'airport_min': AirportDistanceSerializer(airport_min_distance).data })
        else:
          print('No flights', airport, airport_max, airport_min)

    return Response(result)