from django.urls import path
from . import views

urlpatterns = [
  path('airport/', views.AirportListCreate.as_view()),
  path('flights/', views.FlightListCreate.as_view()),
  path('webservices/', views.WebserviceListCreate.as_view()),
  path('ariportsByState/', views.AirportByState.as_view()),
  path('questao3/', views.Questao3.as_view()),
]