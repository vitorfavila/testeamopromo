from rest_framework import serializers
from .models import Airport, Aircraft, Webservice, Flight, AirportDistance
from django.db import models

class WebserviceSerializer(serializers.ModelSerializer):
  class Meta:
    model = Webservice
    fields = ('name', 'provider', 'url', 'username', 'password', 'api_key')

class AirportSerializer(serializers.ModelSerializer):
  class Meta:
    model = Airport
    fields = ('iata', 'city', 'lat', 'lon', 'state', 'created_at')

class AircraftSerializer(serializers.ModelSerializer):
  class Meta:
    model = Aircraft
    fields = ('model', 'manufacturer', 'avg_speed', 'avg_cost_km')

class AirportDistanceSerializer(serializers.ModelSerializer):
  airport_from = AirportSerializer(read_only=True)
  airport_to = AirportSerializer(read_only=True)

  class Meta:
    model = AirportDistance
    fields = ('airport_from', 'airport_to', 'distance')

class FlightSerializer(serializers.ModelSerializer):
  airport_from = AirportSerializer(read_only=True)
  airport_to = AirportSerializer(read_only=True)
  airport_dist = AirportDistanceSerializer(read_only=True)
  aircraft = AircraftSerializer(read_only=True)

  class Meta:
    model = Flight
    fields = ('id', 'airport_from', 'airport_to', 'departure_time', 'arrival_time', 'fare_price', 'aircraft', 'airport_dist')

class AirportByStateSerializer(serializers.ModelSerializer):
  scount = serializers.IntegerField()

  class Meta:
    model = Airport
    fields = ('state', 'scount')