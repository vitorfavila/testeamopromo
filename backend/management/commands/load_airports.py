from django.core.management.base import BaseCommand
from backend.services import get_airports
import json, ast
# from pprint import pprint
from backend.models import Airport, AirportDistance
from haversine import haversine

class Command(BaseCommand):
  help = 'Load API Mockup Airports'

  def handle(self, *args, **kwargs):
    data = get_airports()

    for key, airport in data.items():
      try:
        existingAirport = Airport.objects.get(iata=airport['iata'])
        existingAirport.iata = airport['iata']
        existingAirport.city = airport['city']
        existingAirport.lat = str(airport['lat'])
        existingAirport.lon = str(airport['lon'])
        existingAirport.state = airport['state']
        existingAirport.save()
      except (Airport.DoesNotExist):
        newAirport = Airport.objects.create(iata=airport['iata'], city=airport['city'], lat=str(airport['lat']), lon=str(airport['lon']), state=airport['state'])

    airports = Airport.objects.all()
    for airport1 in airports[0:20]:
      for airport2 in airports[0:20]:
        if airport1.id != airport2.id:
          distance = haversine((airport1.lat, airport1.lon), (airport2.lat, airport2.lon))
          obj, created = AirportDistance.objects.update_or_create(
            airport_from=airport1, airport_to=airport2,
            defaults={'distance': f'{distance:.2f}'},
          )
          print('Loaded', airport1.iata, airport2.iata, distance)
    
    print('ALL AIRPORTS LOADED')
