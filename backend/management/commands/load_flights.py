from django.core.management.base import BaseCommand
from backend.services import get_flights, create_flights_import
from pprint import pprint
from datetime import datetime
from decimal import Decimal
from backend.models import Webservice, Airport, Aircraft, Flight, AirportDistance, FlightImport, FlightImportRoute

def Average(lst):
  try:
    return sum(lst) / len(lst) 
  except ZeroDivisionError:
    return 0
class Command(BaseCommand):
  help = 'Load API Mockup Flights'

  def handle(self, *args, **kwargs):
    aircrafts_speed = list()
    aircrafts_cost_km = list()
    aircrafts_db = Aircraft.objects.all()

    for aircraft in aircrafts_db:
      aircrafts_speed.append({aircraft.id: []})
      aircrafts_cost_km.append({aircraft.id: []})

    checkNewImport = False
    try:
      flightImport = FlightImport.objects.latest()
    except (FlightImport.DoesNotExist):
      checkNewImport = True
      flightImport = create_flights_import()

    routes = FlightImportRoute.objects.filter(flight_import=flightImport, has_ended=False)
    if flightImport.has_ended == True:
      print('Last import ID {} was successfully loaded.'.format(flightImport.id))
      print('Creating a new Import')
      flightImport = create_flights_import()
      routes = FlightImportRoute.objects.filter(flight_import=flightImport)
    elif checkNewImport == True:
      print('Starting from scratch - Creating a new Import')
    else:
      print("Continuing import ID {}, started at {}".format(flightImport.id, flightImport.created_at))

      # CLEAN INCOMPLETE ROUTE
      incompleteRoute = FlightImportRoute.objects.filter(flight_import=flightImport, has_ended=False).earliest()
      incompleteRouteFlights = Flight.objects.filter(airport_from=incompleteRoute.airport_from_id, airport_to=incompleteRoute.airport_to_id)
      if incompleteRouteFlights.count() > 0:
        incompleteRouteFlights.delete()

    for route in routes:
      print('In progress...')
      airport = route.airport_from
      secAirport = route.airport_to

      if airport.id != secAirport.id:
        try:
          airportDistance = AirportDistance.objects.filter(airport_from=airport, airport_to=secAirport)[0]
        except IndexError:
          print('MISSING AIRPORT DISTANCE BETWEEN', airport.id, secAirport.id)

        data = get_flights(airport.iata, secAirport.iata)
        summary = data['summary']
        options = data['options']

        airport_from = Airport.objects.get(iata=summary['from']['iata'])
        airport_to = Airport.objects.get(iata=summary['to']['iata'])
        
        for flightOption in options:
          # print(flightOption)
          aircraft_model = flightOption['aircraft']['model']
          aircraft_manufacturer = flightOption['aircraft']['manufacturer']

          # AIRCRAFT
          try:
            aircraft = Aircraft.objects.get(model=aircraft_model, manufacturer=aircraft_manufacturer)
          except (Aircraft.DoesNotExist):
            aircraft = Aircraft.objects.create(model=aircraft_model, manufacturer=aircraft_manufacturer)
            aircrafts_speed.append({aircraft.id: []})
            aircrafts_cost_km.append({aircraft.id: []})

          # Totalizadores
          departure_datetime = datetime.strptime(flightOption['departure_time'], '%Y-%m-%dT%H:%M:%S')
          arrival_datetime = datetime.strptime(flightOption['arrival_time'], '%Y-%m-%dT%H:%M:%S')
          
          flight_duration = (arrival_datetime-departure_datetime).total_seconds() / 3600

          # AVG SPEED
          for aircraft_item in aircrafts_speed:
            for key in aircraft_item:
              if key == aircraft.id:
                aircraft_item[key].append(airportDistance.distance / Decimal(flight_duration))
          # KM COST
          for aircraft_item in aircrafts_cost_km:
            for key in aircraft_item:
              if key == aircraft.id:
                aircraft_item[key].append(Decimal(flightOption['fare_price']) / airportDistance.distance)

          # FLIGHT
          try:
            flight = Flight.objects.get(
              airport_from=airport_from, 
              airport_to=airport_to, 
              departure_time=flightOption['departure_time'], 
              arrival_time=flightOption['arrival_time'],
              aircraft=aircraft,
              airport_dist=airportDistance
            )

            flight.fare_price = flightOption['fare_price']
            flight.save()
          except (Flight.DoesNotExist):
            flight = Flight.objects.create(
              airport_from=airport_from, 
              airport_to=airport_to, 
              departure_time=flightOption['departure_time'], 
              arrival_time=flightOption['arrival_time'],
              fare_price=flightOption['fare_price'],
              aircraft=aircraft,
              airport_dist=airportDistance
            )
      route.has_ended = True
      route.save()

    flightImport.has_ended = True
    flightImport.save()

    for aircraft_speed_list in aircrafts_speed:
      for key in aircraft_speed_list:
        aircraft = Aircraft.objects.get(pk=key)
        aircraft.avg_speed = Average(aircraft_speed_list.get(key, []))
        aircraft.save()

    for aircraft_cost_list in aircrafts_cost_km:
      for key in aircraft_cost_list:
        aircraft = Aircraft.objects.get(pk=key)
        aircraft.avg_cost_km = Average(aircraft_cost_list.get(key, []))
        aircraft.save()

    print('ALL FLIGHTS LOADED')

      