from django.core.management.base import BaseCommand
# from pprint import pprint
from backend.models import Webservice

class Command(BaseCommand):
  help = 'Populate Webservice table'

  def handle(self, *args, **kwargs):
    Webservice.objects.create(
      name="airports", 
      provider="amopromo", 
      url="http://stub.2xt.com.br/air/airports/rkiwlzxSLwlzwbbJFnvOWEQMvOWvjj43", 
      username="vitor", 
      password="uhewlz",
      api_key="rkiwlzxSLwlzwbbJFnvOWEQMvOWvjj43"
      )

    Webservice.objects.create(
      name="flights", 
      provider="amopromo", 
      url="http://stub.2xt.com.br/air/search", 
      username="vitor", 
      password="uhewlz",
      api_key="rkiwlzxSLwlzwbbJFnvOWEQMvOWvjj43"
      )
    
    print('WEBSERVICES ADDED')
