import requests
from backend.config import get_days_for_travel
from datetime import date, timedelta
from backend.models import Webservice, Airport, FlightImport, FlightImportRoute, Flight
from django.db import connection

def create_flights_import():
  flightImport = FlightImport.objects.create()
  generate_airpots_flights_import(flightImport)

  # CLEANUP
  Flight.objects.all().delete()

  return flightImport

def generate_airpots_flights_import(flight_import):
  airports = Airport.objects.all();

  for airport in airports[0:20]:
    for secAirport in airports[0:20]:
      if airport.id != secAirport.id:
        importItem = FlightImportRoute.objects.create(flight_import=flight_import, airport_from=airport, airport_to=secAirport)

def get_airports():
  webservice_name = 'airports'
  webservice = Webservice.objects.filter(name=webservice_name)[0]
  
  r = requests.get(webservice.url, auth=(webservice.username, webservice.password))

  return r.json()

def get_flights(aiport_from, airport_to):
  webservice_name = 'flights'
  webservice = Webservice.objects.filter(name=webservice_name)[0]
  target_date = date.today() + timedelta(days=get_days_for_travel())
  apiUrl = webservice.url + '/{}/{}/{}/{}'.format(webservice.api_key, aiport_from, airport_to, target_date.strftime("%Y-%m-%d"))
  
  r = requests.get(apiUrl, auth=(webservice.username, webservice.password))
  # print(r.status_code)
  data = r.json()
  return data

def get_airport_closer(airport_from):
  with connection.cursor() as cursor:
    cursor.execute("""
      SELECT
        airport_to.id as airport_id
      FROM
        backend_airportdistance a
      INNER JOIN (
        SELECT airport_from_id, min(distance) dist FROM backend_airportdistance GROUP BY airport_from_id
      ) b ON b.airport_from_id = a.airport_from_id AND b.dist = a.distance
      LEFT JOIN backend_airport airport
        ON airport.id = a.airport_from_id
      LEFT JOIN backend_airport airport_to
        ON airport_to.id = a.airport_to_id
      WHERE a.airport_from_id = %s
    """, [airport_from])
    row = cursor.fetchone()

  return row

def get_airport_further(airport_from):
  with connection.cursor() as cursor:
    cursor.execute("""
      SELECT
        airport_to.id as airport_id
      FROM
        backend_airportdistance a
      INNER JOIN (
        SELECT airport_from_id, max(distance) dist FROM backend_airportdistance GROUP BY airport_from_id
      ) b ON b.airport_from_id = a.airport_from_id AND b.dist = a.distance
      LEFT JOIN backend_airport airport
        ON airport.id = a.airport_from_id
      LEFT JOIN backend_airport airport_to
        ON airport_to.id = a.airport_to_id
      WHERE a.airport_from_id = %s
    """, [airport_from])
    row = cursor.fetchone()

  return row