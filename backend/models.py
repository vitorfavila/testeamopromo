# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Webservice(models.Model):
  name      = models.CharField(max_length=20)
  provider  = models.CharField(max_length=80)
  url       = models.CharField(max_length=200)
  username  = models.CharField(max_length=20)
  password  = models.CharField(max_length=20)
  api_key   = models.CharField(max_length=200)

class Airport(models.Model):
  iata        = models.CharField(max_length=3)
  city        = models.CharField(max_length=100)
  lat         = models.DecimalField(max_digits=22, decimal_places=16)
  lon         = models.DecimalField(max_digits=22, decimal_places=16)
  state       = models.CharField(max_length=2)
  created_at  = models.DateTimeField(auto_now_add=True)

  class Meta:
    ordering = ['iata']

class Aircraft(models.Model):
  model         = models.CharField(max_length=100)
  manufacturer  = models.CharField(max_length=100)
  avg_speed     = models.DecimalField(max_digits=6, decimal_places=2, default=0)
  avg_cost_km   = models.DecimalField(max_digits=6, decimal_places=2, default=0)

class AirportDistance(models.Model):
  airport_from  = models.ForeignKey(Airport, related_name='airport_distance_from', on_delete=models.CASCADE)
  airport_to    = models.ForeignKey(Airport, related_name='airport_distance_to', on_delete=models.CASCADE)
  distance      = models.DecimalField(max_digits=22, decimal_places=16)

class Flight(models.Model):
  airport_from    = models.ForeignKey(Airport, related_name='airport_from', on_delete=models.CASCADE)
  airport_to      = models.ForeignKey(Airport, related_name='airport_to', on_delete=models.CASCADE)
  departure_time  = models.DateTimeField()
  arrival_time    = models.DateTimeField()
  fare_price      = models.DecimalField(max_digits=8, decimal_places=2)
  aircraft        = models.ForeignKey(Aircraft, on_delete=models.CASCADE)
  airport_dist    = models.ForeignKey(AirportDistance, on_delete=models.CASCADE, default=0)

class FlightImport(models.Model):
  created_at  = models.DateTimeField(auto_now_add=True)
  has_ended   = models.BooleanField(default=False)

  class Meta:
    get_latest_by = "created_at"

class FlightImportRoute(models.Model):
  flight_import   = models.ForeignKey(FlightImport, on_delete=models.CASCADE)
  airport_from    = models.ForeignKey(Airport, related_name='airport_import_from', on_delete=models.CASCADE)
  airport_to      = models.ForeignKey(Airport, related_name='airport_import_to', on_delete=models.CASCADE)
  started_at      = models.DateTimeField(null=True)
  has_ended       = models.BooleanField(default=False)

  class Meta:
    get_latest_by = "id"
