FROM python:3
ENV PYTHONUNBUFFERED 1
ENV DB_NAME postgres
ENV DB_USER postgres
ENV DB_HOST teste-postgres-compose
ENV DB_PORT 5432
ENV DB_PASSWORD amopromo
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
