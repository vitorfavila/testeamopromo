Teste para desenvolvedor FullStack Amo Promo
======

## Tecnologias utilizadas
  * Docker
    * docker-compose
  * PostgreSQL
  * Python
    * Django  
  * Javascript ES6
    * React

## Credenciais
### Django Admin
`root / amopromo`
### PostgreSQL
`vitorfavila@gmail.com / amopromo`

-- --

## Instalação
`git clone https://vitorfavila@bitbucket.org/vitorfavila/testeamopromo.git`

Entre no diretório criado: 
```
cd testeamopromo
docker-compose up
```
> Verifique se o container web subiu corretamente, pois em alguns casos o web sobe antes do pgsq estar up

#### `No caso derros no container web:`
`docker-compose down`
`docker-compose up`
-- --

# Setup Inicial do BD
`docker-compose exec web python manage.py migrate`
### Carregando Webservices
`docker-compose exec web python manage.py load_webservices`
### Carregando Aeroportos
`docker-compose exec web python manage.py load_airports`
### Carregando Vôos
`docker-compose exec web python manage.py load_flights`

### Acesso Web
`http://127.0.0.1:8989`

### Acesso PgAdmin
`http://127.0.0.1:16543`