import React, { Component } from "react";
import { render } from "react-dom";
import moment from 'moment'

function leftPad(number, targetLength) {
  var output = number + '';
  while (output.length < targetLength) {
      output = '0' + output;
  }
  return output;
}

const NavBar = () => {
  return (
    <ul className="uk-tab">
      <li className="uk-active"><a href="#longas">Viagens mais longas</a></li>
      <li><a href="#estados">Estado com mais aeroportos</a></li>
      <li><a href="#questao3">Origens com destinos mais próximos e distantes</a></li>
    </ul>
  )
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      airports: [],
      airportsByState: [],
      flights: [],
      loaded: false,
      loadedFlights: false,
      loadedAirportsByState: false,
      loadedQuestao3: false,
      questao3: []
    };
  }

  componentDidMount() {
    this.loadQuestao3();
    this.loadFlights();
    this.loadAirportsByState();
  }

  loadAirportsByState = () => {
    fetch("api/v1/ariportsByState")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState({ airportsByState: data.results, loadedAirportsByState: true });
      })
  }

  loadQuestao3 = () => {
    fetch("api/v1/questao3")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            questao3: data,
            loadedQuestao3: true
          };
        });
      });
  }

  loadFlights = () => {
    fetch("api/v1/flights?page=1")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        var flights = data.results.map(res => {
          var a = moment(res.departure_time);
          var b = moment(res.arrival_time);
          var duration = `${-a.diff(b, 'hours')}:${leftPad(-a.diff(b, 'minutes')%60, 2)}`;
          res.trip_duration = duration;
          return res;
        })
        this.setState(() => {
          return {
            flights,
            loadedFlights: true
          };
        });
      });
  }

  renderLoader = (text) => {
    return <div><div className="uk-spinner"></div>{text}</div>
  }

  render() {
    const { loadedFlights, airportsByState, loadedAirportsByState, flights, loadedQuestao3, questao3 } = this.state;

    return (
      <div className="uk-container">

        <div style={{ zIndex: 980 }} uk-sticky="bottom: #offset" className="uk-sticy">
          <NavBar />
        </div>
        
        <div id="longas">
          <br/>
          { !loadedFlights ? this.renderLoader('Carregando as 30 viagens mais longas') : (
            <table className="uk-table uk-table-divider uk-table-striped">
              <caption>Viagens mais longas</caption>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Origem</th>
                  <th>Destino</th>
                  <th>Horário de Partida</th>
                  <th>Horário de Chegada</th>
                  <th>Duração</th>
                  <th>Valor</th>
                  <th>Aeronave</th>
                  <th>Distância</th>
                </tr>
              </thead>
              <tbody>
                {flights.map((flight, i) => {
                  if (i < 30) {
                    return (
                      <tr key={flight.id}>
                        <td>{i+1}</td>
                        <td>{ flight.airport_from.city }</td>
                        <td>{ flight.airport_to.city }</td>
                        <td>{ moment(flight.departure_time).format('DD/MM/YYYY HH:mm') }</td>
                        <td>{ moment(flight.arrival_time).format('DD/MM/YYYY HH:mm') }</td>
                        <td>{ flight.trip_duration }</td>
                        <td>R$ { flight.fare_price.replace('.', ',') }</td>
                        <td>Aircraft { flight.aircraft.manufacturer } { flight.aircraft.model }</td>
                        <td>{ parseFloat(flight.airport_dist.distance).toFixed(2) } KM</td>
                      </tr>
                    );
                  }
                })}     
              </tbody>
            </table>
          )}
        </div>

        <div id="estados">
          <br/>
          { !loadedAirportsByState ? this.renderLoader('Carregando Estados com mais aeroportos') : (
            <table className="uk-table uk-table-divider uk-table-striped" style={{ marginTop: 40 }}>
              <caption>Top 3 aeroportos por estado {airportsByState.length}</caption>
              <thead>
                <tr>
                  <th>Estado</th>
                  <th>Quantidade</th>
                </tr>
              </thead>
              <tbody>
                {airportsByState.map((item, i) => {
                  if (i < 3) {
                    return (
                      <tr key={i}>
                        <td>{item.state}</td>
                        <td>{item.scount}</td>
                      </tr>
                    )
                  }
                  
                })}
              </tbody>
            </table>
          )}
        </div>

        <div id="questao3">
          <br/>
          { !loadedQuestao3 ? this.renderLoader('Carregando a parte mais difícil do teste! kkk') : (
            <table className="uk-table uk-table-divider uk-table-striped" style={{ marginTop: 40 }}>
              <caption>Origens e seus destinos mais próximos e mais distantes</caption>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Origem</th>
                  <th>Destino mais próximo</th>
                  <th>Destino mais distante</th>
                </tr>
              </thead>
              <tbody>
                {questao3.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>{i+1}</td>
                      <td>{item.airport_from.iata}</td>
                      <td>{item.airport_min.airport_to.iata} ({parseFloat(item.airport_min.distance).toFixed(2)} KM)</td>
                      <td>{item.airport_max.airport_to.iata} ({parseFloat(item.airport_max.distance).toFixed(2)} KM)</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);